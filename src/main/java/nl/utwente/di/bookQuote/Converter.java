package nl.utwente.di.bookQuote;

public class Converter {
    double getBookPrice(String isbn){
        Integer cels = Integer.parseInt(isbn);
        return cels*(1.8) + 32;
    }
}
